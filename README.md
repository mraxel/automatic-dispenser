# Pet Food Dispenser
#### AKA: Automatic Dispenser
#### AKA: Automatic Dog Feeder

## What it is
This is a program, designed to run on an Arduino, with a W5100 Eithernet Shield
that will dispense some sort of food if a given set of criteria are met. (namely
time and weight requirements)

## Why it exists
I originally made this for a uni assignment, and am now throwing it here for
"Future Reference". (i.e.: Dammit, I know I got it working before, how did I
manage it last time?)

## Running it
Due to the use of low-level code, this will only run in an Arduino Mega 2560. It
uses code that directly addresses the ATmega2560 registers if you want to know
why. It expects an Arduino Ethernet Shield to be attached (Version 1, with the
W5100 chipset. It uses an active buzzer which should be connected to pin 6. 
Finally it also expects a load cell to be attached via a HX711 pre-amp,
connected on pins A0 & A1.

## The Dispenser
The dispenser is designed to feed two different animals/people, at regular
intervals. Once an interval elapses, it will make a sound signal. If a correct
weight is presented to the load cell within 5 seconds of this sound signal it
will dispense a pre-determined amount of food. If both the large and the small
dispense are scheduled at the same time, one of them will occur, and the other
will be ignored.

## The Web Interface
Depending on how it is configured at compile time, it will either connect to
an existing network with a static IP address, or it can act as a DHCP client
and obtain itself an IP address, and take setps to maintain that IP address
(renew its lease). Once on the network it presents a (very) simple HTTP web
control panel, in the usual manner, from port 80. It uses the Bootstrap CDN
to keep the server code small, so if the client browser is not connected to
the internet it will still work, it just might look a little funky.

## The Buzzer
There is a active buzzer attached to pin 6. (You know the kind? You connect
one pin to ground and another to PWM and it makes horrible tones until you
either unplug it or smash it to tiny pieces?) For the sake of your sanity, I
recommend you *don't* use it, unless expressly required. It is capable of
playing two (well known) tunes, but that won't make it sound any nicer.

## CAD Files
Coming soon. Once I work out how "CAD" works with "GIT". All the cad files have
been created using Autodesk Inventor, but the .dwg files used for the laser
cutter are here, along with the .stl file of the small little bracket that
holds the micro server.

## License
This is one of those "I can't be bothered choosing a license" kind of licenses.
Do what you want as long as it doesn't come back to me. I don't want to deal
with warranties, complaints, recommendations, patents, or any other BS. You
can do whatever you see fit with this code as long as none of it is traceable
back to me. (Unless I get around to uploading a LICENSE.md, in which case you
should probably obey that....)