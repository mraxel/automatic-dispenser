﻿/*
 Name:			Dispenser.ino
 Created:		2018-08-18 16:00:52
 Author:		Alexander Gregory (217270644)	
 Unit:			SER202: Programming for Embedded Systems
 Unit Chair:	Professor Abbas Kouzani
 Comments:		nill
*/

/*
The HX711.h library has been used.
The library has been used in accordance with its license, the GNU GPL v2.0
The original license can be found here: https://github.com/bogde/HX711/blob/master/LICENSE
The original repository is available at https://github.com/bogde/HX711

The Ethernet.h library has been used. This library is part of the default set of Arduino libraries.
The library has been used in accordance with its license, the GNU LGPL v2.1.
The original license can be found here: https://github.com/arduino-libraries/Ethernet/blob/master/README.adoc
The original repository is available at https://github.com/arduino-libraries/Ethernet

The SPI.h library has been used as it is required by the Ethernet.h library
The SPI.h library is part of the default set of Arduino libraries

The HTML server utilises the Bootstrap v4.1 CSS library. This library is only used client-side.
This library has been used in accordance with its license, the MIT license.
The original license can be found here: https://github.com/twbs/bootstrap/blob/v4-dev/LICENSE
The original repository is available at https://github.com/twbs/bootstrap

The HTML server utilises the jQuery Javascript library. This library is only used client-side.
This library has been used in accordance with its license, the MIT license.
The original license can be found here: https://github.com/jquery/jquery/blob/master/LICENSE.txt
The original repository is available at https://github.com/jquery/jquery

The HTML server utilises the Popper.js Javascript library. This library is only used client-side.
This library has been used in accordance with its license, the MIT license.
The original license can be found here: https://github.com/FezVrasta/popper.js/blob/master/LICENSE.md
The original repository is available at https://github.com/FezVrasta/popper.js
*/

/*
Timer 0 is used used the Arduino IDE
Timer 1 is used to count seconds (replacement for delay() and millis()), as well as timing the servo.
Timer 2 is used by the SPI & Ethernet libraries for the web server
Timer 3 is used by the HX711 library for the load cell
Timer 4 is used for tone generation of the buzzer.
Timer 5 is used to time the reset period
*/

// Include libraries required for the ethernet shield
#include <SPI.h>		/// For Ethernet Shield
#include <Ethernet.h>	/// For Ethernet Shield
#include <HX711.h>		/// For Load Cell Amp

// Feeding size definitions
#define LOAD_LG 25
#define LOAD_ML 53
#define LOAD_MD 78
#define LOAD_MS 85
#define LOAD_SM 104
#define LOAD_RESET 141

// Buzzer Beat/Time definitions
#define BPM 140			// BPM, Beats per Minute. Only accepts numbers (sorry, not "Allegro").
#define BEAT 6000/BPM	// Converts "BPM" into an amount of time
#define DQ BEAT / 8		// Semi-Demi Quaver (𝅘𝅥𝅰)
#define SQ BEAT / 4		// Semi-Quaver (𝅘𝅥𝅯)
#define QU BEAT / 2		// Quaver (𝅘𝅥𝅮)
#define DQ BEAT / 1.15	// Dotted Quaver
#define CT BEAT			// Crotchet (𝅘𝅥)
#define DC BEAT * 1.5	// Dotted Crotchet
#define MN BEAT * 2		// Minim (𝅗𝅥)
#define SM BEAT * 4		// Semibreve (𝅝)

// Buzzer Note/Pitch definitions (Manually tuned. RIP my ears)
#define A2  569
#define A2S 536
#define B2  506
#define C3  478
#define C3S 451
#define D3  426
#define D3S 402
#define E3  379
#define F3  358
#define F3S 338
#define G3  319
#define G3S 301
#define A3  284
#define A3S 268
#define B3  253
#define C4  239
#define C4S 225
#define D4  213
#define D4S 201
#define E4  190
#define F4  179
#define F4S 169
#define G4  159
#define G4S 150
#define A4  140
#define A4S 133
#define B4  126
#define C5  119
#define C5S 112
#define D5  106
#define D5S 100
#define E5  94
#define F5  89
#define F5S 84
#define G5  79
#define G5S 75
#define A5  71
#define A5S 67
#define B5  63
#define C6  59
#define C6S 56
#define D6  53
#define D6S 50
#define E6  47
#define F6  45
#define F6S 42
#define G6  40
#define G6S 38

// Scale callibration
#define SCALE_CALLIBRATION 364

// Would you like DHCP or a static IP address?
#define DHCP false

// Serial comms stream (used for printf & scanf)
static FILE uart00 = { 0 };

// Network Configuration
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
byte ip[] = { 192, 168, 2, 34 };
byte gateway[] = { 192, 168, 1, 1 };
byte subnet[] = { 255, 255, 255, 0 };

// This establishes the Ethernet Shield as a server device
EthernetServer server(80);

// Extra stuff for networking (This handles the POST requests)
char response[1024];
int i = 0;

// Variables for the (timing of the) dog-feeder function
volatile bool feed = 0;					/// is TRUE if a feed is currently underway
unsigned int feed_delay_small = 50;		/// How long is it between small feeds?
unsigned int feed_delay_large = 90;		/// How long is it between large feeds?
unsigned int feed_elapsed_small = 0;	/// How long since the last small feed?
unsigned int feed_elapsed_large = 0;	/// How long since the last large feed?
unsigned int feed_window_small = 0;		/// How long till cancelling small feed?
unsigned int feed_window_large = 0;		/// How long till cancelling large feed?
volatile uint8_t temp = 0;				/// Used to keep track of which cycle is a second
volatile unsigned long time = 0;		/// This is used to keep track of the current time (seconds).
unsigned long oldTime = 0;				/// This is used to keep track of the passage of time

// Variables for the buzzer function
const uint8_t note[2][15] = { {G4,G4,G4,D4,B4,B4,B4,G4,G4,B4,D5,D5,C5,B4,A4},{B4,D5,B4,E5,D5,D5,0, D5,C5S,B4} };
const uint8_t beat[2][15] = { {QU,QU,CT,CT,QU,QU,CT,CT,QU,QU,DC,QU,QU,QU,MN},{QU,QU,QU,DQ,SQ,MN,CT,CT, CT,CT} };
const uint8_t song[2] = { (sizeof(note[0]) / sizeof(note[0][0])), (sizeof(note[1]) / sizeof(note[1][0])) };	// Will be populated in setup(), then never changed after that.

// This sets up the scale for measuring weight
HX711 scale(A0, A1);					/// Connect pin A0 to DT (D-OUT) and pin A1 to SCK.
uint16_t weight = 0;					/// Used to store the weight measured by the load cell.
uint16_t weight_small = 250;			/// Used for comparing to how heavy the small animal is.
uint16_t weight_large = 750;			/// Used for comparing to how heavy the large animal is.
uint16_t weight_margin = 150;			/// How much above/below a weight can be from small or large to still register as "ok".

void setup() {
	setup_scale();
	setup_timers();
	setup_serial();
	printf("\n=========================\n[STATUS] Program Started.\n[STATUS] Made by Alexander Gregory\n[STATUS] for SER202: Programming for Embedded Systems");
	setup_server(DHCP);
}

void loop() {
	// Check that the web server is still working
	if (server) {
		if ((time != oldTime) && DHCP) {
			// Check with the DHCP server that our IP is still valid, only if we DHCP is enabled and enough time has elapsed since we last checked
			//server_renew();
		}
	}
	else {
		setup_server(DHCP);
	}

	// Check if there are any pending GET requests, and act upon them...      (also checks POST requests)
	server_http();

	// Perform Feeds
	feeder_check();		/// Check to see how much time has elapsed and scheduled any due feeds
	feeder_execute();	/// Check if any feed have been manually triggered, or if any are already running, and trigger scheduled feeds from feeder_check();
}

// FUNCTION:	setup_scale
// INPUTS:		nill
// OUTPUTS:		nill
// RETURNS:		nill
// PURPOSE:		Uses HX711.h library to establish connection to the HX711 amp & load cell
void setup_scale() {
	// Start and callibrate the load cell (scales)
	scale.set_scale(SCALE_CALLIBRATION);
	scale.tare();
}

// FUNCTION:	setup_timers
// INPUTS:		nill
// OUTPUTS:		nill
// RETURNS:		nill
// PURPOSE:		Sets the internal counter/timers to be used as a 'stopwatch'/'timer' to track time between feeds and
//                fire interrupts when time has elapsed and it is ready for a feed.
void setup_timers() {
	cli();												/// Clear interrupts to prevent problems with our registers while setting them

	// Setup the seconds & servo timer
	DDRB |= (1 << DDB6);								/// Enable servo output on Port B6 (Pin 12)
	TCCR1A = (1 << COM1B1) | (1 << WGM11);				/// Set the timer to Fast-PWM, and enable PWM on Port B6 (Pin 12).
	TCCR1B = (1 << WGM13) | (1 << WGM12) | (1 << CS12);	/// Set the pre-scaler to divide by 256. (Makes the timer increment slower), and set to fast PWM
	TCCR1C = 0;											/// Don't force any compare matches
	TIMSK1 = (1 << TOIE1);								/// Enable this timer's overflow interrupt (The timing part of this timers function)
	ICR1 = 1250;										/// Set 1250 as the maximum counting value (for 50Hz, PWM frequency)
	OCR1B = LOAD_RESET;									/// Set the default servo position into Output Compare Register B
	TCNT1 = 0;											/// Load 0 into the timer, to start counting from the bottom

	// Setup the feeder reset timer
	TCCR5A = 0;											/// Set the timer to normal mode and don't do anything on a compare match
	TCCR5B = (1 << CS52) | (1 << CS50);					/// Set the pre-scaler to divide by 1024 (Makes the timer increment slower).
	TCCR5C = 0;											/// Don't force any compare matches
	TIMSK5 = 0;											/// Disable the interrupt overflow for timer/counter 5A & 5B
	ICR5 = 0xFFFF;										/// Set TOP to it's maximum value.
	OCR5A = 0x8FFF;										/// Load the first value into the output compare register for the dispenser reset
	OCR5B = 0xDFFF;										/// Load the second value into the output compare register for the flag clear
	TCNT5 = 0x0000;										/// Load 0 into the timer, to start counting from there (4.19425s to TOP)

	// Setup the music timer
	DDRH |= (1 << DDH3);								/// Connect the buzzer to Pin 6.
	TCCR4A = (1 << COM4A1) | (1 << WGM41);				/// Set the timer to Fast PWM mode, and connect timer to PORT H3 (Pin 6)
	TCCR4B = (1 << WGM43) | (1 << WGM42) | (1 << CS42);	/// Set the timer to Fast PWM mode, and set the prescaler to /256
	TCCR4C = 0;											/// Don't force compare matches
	TIMSK4 = 0;											/// Don't enable any interrupts
	ICR4 = 0;											/// Set the max value to 0, (basically turning the timer off). Used for frequency control.

	// We're done here, tidy up after ourselves
	sei();													/// Re-enable interrupts now that we are finished.
}

 // FUNCTION:	setup_server
 // INPUTS:		MAC address (as a size-12 array or char's), DHCP (bool)
 // OUTPUTS:	nill
 // RETURNS:	nill
 // PURPOSE:	Establishes a connection to the Ethernet shield and sets the shield to Server mode.
void setup_server(bool dhcp) {
	// The shield can run into trouble if the SD card reader tries to use the interface, so we should disable
	//   the SD card reader by pulling pin 4 high.
	DDRG = (1 << DDG5);
	PORTG = (1 << PORTG5);

	// Initialise the shield on pin 10
	Ethernet.init(10);
	
	// Start the Ethernet server, and request an IP from the DHCP server. Print reasons for error if that fails.
	printf("\n[SERVER] Starting Server.");
	if (dhcp) {
		// if DHCP is called, then run without an IP address, and request it from the DHCP server
		if (Ethernet.begin(mac) == 0) {
			printf("\n[SERVER] Failed to get a IP address from the DHCP server, reason: ");
			if (Ethernet.hardwareStatus() == EthernetNoHardware) {
				Serial.println("ethernet shield was not found.");
			}
			else if (Ethernet.linkStatus() == LinkOFF) {
				Serial.println("ethernet cable is not connected.");
			}
			printf("\n[SERVER] ERROR: Server failed to load.");
		}
		else {
			// If there are no errors, start the server and print the IP address to the serial console.
			server.begin();
			printf("\n[SERVER] Server is running at %s:80", server_ip());
		}
	}
	else {
		// If DHCP is not called, then use a static IP set in definitions
		Ethernet.begin(mac, ip);

		// Check for any errors:
		if (Ethernet.hardwareStatus() == EthernetNoHardware) {
			Serial.println("\n[SERVER] Failed to start, reason: ethernet shield was not found.");
		}
		else if (Ethernet.linkStatus() == LinkOFF) {
			Serial.println("\n[SERVER] Failed to start, reason: ethernet cable is not connected.");
		}
		// Start the server and let the user know what the IP is using the serial console.
		server.begin();
		printf("\n[SERVER] started on %s:80", server_ip());
	}
}

// FUNCTION:	setup_serial
// INPUTS:		nill
// OUTPUTS:		nill
// RETURNS:		nill
// PURPOSE:		Sets up the USART0 register to be used for serial communication
void setup_serial() {
	UCSR0A = 0x00;		/// Clear flags to be ready for TX and RX
	UCSR0B = 0x18;		/// Enable TX & RX, no interrupts
	UCSR0C = 0x06;		/// 8-bit packet, 1 stop-bit, no parity, asynchronous
	UBRR0 = 8;			/// Set baud to 115200
	fdev_setup_stream(&uart00, TX, RX, _FDEV_SETUP_RW);
	stdin = stdout = &uart00;
}

// FUNCTION:	feeder_check
// INPUTS:		"time"
// OUTPUTS:		"feed" flags
// RETURNS:		nill
// PURPOSE:		Checks the time to see if an action needs to be preformed, and then triggers that action.
void feeder_check() {
	// Check if a small feed needs to occur. If one needs to occur (if the time since the last scheduled feed
	//   is greater than the amount interval time between feeds), then reset the time since the last scheduled
	//   feed, play the sound signal, and set the amount of time that the dog has to collect his meal. Every
	//   second, the "window" vars will drop by one. A feed will only dispense if the following conditions are met:
	//     1) The "Window" variable for that animal (large/small) is positive and not zero
	//     2) The scales/load cell returns the correct weight for the respective animal (large/small)
	//     3) A feed is not already underway (the "feed" flag is not set).
	//   This function will set criteria (1) above. Criterias (2) and (3) are check by the function feeder_execute().

	// Check if a small feed needs to occur.
	if ((time - feed_elapsed_small) > feed_delay_small) {
		feed_elapsed_small = time;					/// Reset the time until next dispense check
		printf("\n[FEEDER] Small scheduled");		/// Display a debug message to the console user
		feeder_music(0);							/// Play a tune to indicate rediness for dispensinig action
		feed_window_small = 5;						/// Set a timeframe for which to dispense food inside of.
	}
	// Check if a large feed needs to occur. Steps are the same for "large" as "small".
	if ((time - feed_elapsed_large) > feed_delay_large) {
		feed_elapsed_large = time;
		printf("\n[FEEDER] Large scheduled");
		feeder_music(1);
		feed_window_large = 5;
	}
}

// FUNCTION:	feeder_execute
// INPUTS:		"feed" flags
// OUTPUTS:		feeder_dispense(), feeder_music()
// RETURNS:		nill
// PURPOSE:		Checks the "feed" flags
void feeder_execute() {
	// Ensure that the feeding windows are updated
	timer_update();
	
	// Perform a weight reading from the scales, but only when there is a scheduled feed, so we aren't continuously
	//   polling the scales/HX711 sensor.
	if ((feed_window_small > 0) || (feed_window_large > 0)) {
		weight = scale.get_units();
	}

	if(!feed) {
		// If "feed" is true, then a feed is underway. If it is false then this IF statement will execute, which will check
		//   if a feed should occur, and set "feed" to true. We don't need to worry about setting "feed" to false once a
		//   feed is complete, the timer responsible for managing the servo & servo PWM will automatically clear it for us.
		// The IF statement is so long because it does three things: Feed flag (window), weight is not above or below the max
		//   margins. The margens are calculated by adding or subtracting the margin value to the weight value.
		if ((feed_window_small > 0) && (weight > (weight_small - weight_margin)) && (weight < (weight_small + weight_margin))) {
			// Check if a small feed needs to occur. It needs to occur if both "feed_window_small" is nonzero, and if the weight
			//   sensor is returning the correct weight for a small feed. The if statement will check that both of these
			//   conditions are met, and, if they are, it will trigger a food dispense by calling feeder_dispense(), and clear
			//   the feed window counter variable so that only one feed happens per setting of the feed window.
			feeder_dispense(3);
			feed_window_small = 0;
			printf("\n[WEIGHT] Measured weight is %d", weight);
		}
		// Check if a large feed needs to occur. Uses "else if" so it won't execute a small and a large at the same time.
		else if ((feed_window_large > 0) && (weight > (weight_large - weight_margin)) && (weight < (weight_large + weight_margin))) {
			feeder_dispense(5);
			feed_window_large = 0;
			printf("\n[WEIGHT] Measured weight is %d", weight);
		}
	}
}

// FUNCTION:	timer_update
// INPUTS:		time
// OUTPUTS:		feed_window_small, feed_window_large
// RETURNS:		nill
// PURPOSE:		Each second, decrement the feed window by 1 until it's 0.
void timer_update() {
	// This evaluates to true if another second has passed
	if (time != oldTime) {
		oldTime = time;
		// Only reduce the time window if the value is larger than 0.
		if (feed_window_large > 0) {
			feed_window_large--;
		}
		if (feed_window_small > 0) {
			feed_window_small--;
		}
	}
}

// FUNCTION:	feeder_dispense
// INPUTS:		size
// OUTPUTS:		timer 1
// RETURNS:		nill
// PURPOSE:		Triggers a dispense of food, and will reset when finished.
void feeder_dispense(uint8_t size) {
	// No feed is currently occuring, proceed with feed.

	// Set the bit that a feed is occuring so that we won't overwrite it.
	feed = true;
	printf("\n[FEEDER] Feed triggered (size %d)", size);

	// Dispense the correct size...
	switch (size) {
	case 0:
		OCR1B = LOAD_RESET;
		break;
	case 1:
		OCR1B = LOAD_SM;
		break;
	case 2:
		OCR1B = LOAD_MS;
		break;
	case 3:
		OCR1B = LOAD_MD;
		break;
	case 4:
		OCR1B = LOAD_ML;
		break;
	case 5:
		OCR1B = LOAD_LG;
		break;
	default:
		printf("\n[ERROR] Feeder was not given size parameter");
		break;
	}

	// Set the timers and enable the interrupts that will finish the dispenser and clear the flag.
	TCNT5 = 0x0000;											/// Set timer to 0 so it starts counting with the full cycle.
	TIFR5 |= (1 << OCF5A) | (1 << OCF5B);					/// Clear any pending interrupts, so they won't fire the instant we enable them
	TIMSK5 |= (1 << OCIE5A) | (1 << OCIE5B) | (1 << TOIE5);	/// Enable interrupts to fire on compare mathes (in timer 5)
}

// FUNCTION:	feeder_music
// INPUTS:		tune to play
// OUTPUTS:		timer4
// RETURNS:		nill
// PURPOSE:		Plays a given tune.
void feeder_music(uint8_t tune) {
	int i, j;

	// Loop through each note in the song, and play it.
	// ATTENTION: THIS IS BLOCKING CODE! OTHER FUNCTIONS WILL NOT RUN WHILE THIS CODE IS RUNNING!
	for (i = 0; i < song[tune]; i++) {
		ICR4 = note[tune][i];									/// Set TOP to the frequency
		OCR4C = note[tune][i] / 2;								/// Set compare to half the frequnecy (this can be adjusted for volume).
		TCNT4 = 0;												/// Reset the timer
		for (j = 0; j < beat[tune][i]; j++) { _delay_ms(10); }	/// Delay for the duration of the note.
	}

	// Once finished, turn the timer off to stop it making noise.
	ICR4 = 0; OCR4C = 0; TCNT4 = 0;
}

// FUNCTION:	server_renew
// INPUTS:		nill
// OUTPUTS:		nill
// RETURNS:		nill
// PURPOSE:		Re-check the DHCP server to check current IP is still valid and, if needed, renew IP.
void server_renew() {
	switch (Ethernet.maintain()) {
	case 1:
		// The renew failed.
		printf("\n[SERVER] ERROR: DHCP IP renew failed.");
		break;
	case 2:
		// The renew succeeded.
		printf("\n[SERVER] DHCP IP renew succeeded.");
		printf("\n[SERVER] New IP address is %s", server_ip());
		break;
	case 3:
		// Failed to re-bind to old IP
		printf("\n[SERVER] ERROR: DHCP IP rebind failed.");
		break;
	case 4:
		// Rebind succeded
		printf("\n[SERVER] DHCP IP rebind succeeded.");
		printf("\n[SERVER] New IP address is %s", server_ip());
		break;
	default:
		// No action needed to be taken...
		break;
		// (so we did nothing).
	}
}

// FUNCTION:	server_ip
// INPUTS:		Ethernet.localIP();
// OUTPUTS:		nill
// RETURNS:		Current IP addresses, formatted as a string
// PURPOSE:		Creates a string containing the current IP address, ready to be printed to screen or serial console.
const char* server_ip() {
	static char buffer[16];				/// Somewhere to store the finished string
	uint32_t ip = Ethernet.localIP();	/// Get the IP address into a variable we can point to
	uint8_t *ipt = (uint8_t*)&ip;		/// Create a pointer to each section of the IP address
	sprintf(buffer, "%d.%d.%d.%d\0", ipt[0], ipt[1], ipt[2], ipt[3]);	/// Form the finsiehd string
	return buffer;						/// Return the finished string to be used &/or printed.
}

// FUNCTION:	http200
// INPUTS:		nill
// OUTPUTS:		nill
// RETURNS:		nill
// PURPOSE:		Send a formatted HTTP 200 response to a connected client via the ethernet shield
void server_http200(EthernetClient client) {
	// Convert the times displayed from sss into mm:ss
	uint8_t time_s = time % 60;
	uint8_t time_m = time / 60;

	/// HEADS UP! We are now transitinging from ANSI C language, to HTML5. There will also be some CCS3 and Javascript below.
	/// That's the game we're playing here: 4 computer languages in 1 file. Gangsta.
	client.println("HTTP/1.1 200 OK");
	client.println("Content-Typoe: text/html");
	client.println("Connection: close");
	client.println("Refresh: 30");
	client.println();
	client.println("<!DOCTYPE HTML>");
	client.println("<html>");
	client.println("  <head>");
	client.println("    <title>Dispenser Controller</title>");
	client.println("    <link rel=\"icon\" href=\"https://www.arduino.cc/favicon.ico\">");
	client.println("    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">");
	client.println("  </head>");
	client.println("  <body>");
	client.println("    <div class=\"jumbotron jumbotron-fluid bg-warning\">");
	client.println("      <div class=\"container\">");
	client.println("        <h1 class=\"display-3\">Pet Food Dispenser Control</h1>");
	client.println("        <p>This page has been created by a W5100 Ethernet Shield connected to an Arduino Mega 2560 R3</p>");
	client.println("      </div>");
	client.println("    </div>");
	client.println("    <div class=\"container my-5\">");
	client.println("      <h3>Current Status</h3>");
	client.println("      <div class=\"row\">");
	client.println("        <div class=\"col-md-4\">");
	client.println("          <ul class=\"list-group\">");
	client.println("            <li class=\"list-group-item d-flex justify-content-between align-items-center bg-warning\">System Stats</li>");
	client.print("            <li class=\"list-group-item d-flex justify-content-between align-items-center\">Current system time<span class=\"badge badge-warning badge-pill\">"); client.print(time_m); client.print(":"); client.print(time_s); client.println("</span></li>");
	client.print("            <li class=\"list-group-item d-flex justify-content-between align-items-center\">Current weight reading<span class=\"badge badge-warning badge-pill\">"); client.print(scale.get_units()); client.println("</span></li>");
	client.println("          </ul>");
	client.println("        </div>");
	client.println("        <div class=\"col-md-4\">");
	client.println("          <ul class=\"list-group\">");
	client.println("            <li class=\"list-group-item d-flex justify-content-between align-items-center bg-warning\">Small Dog</li>");
	client.print("            <li class=\"list-group-item d-flex justify-content-between align-items-center\">Time between feeds:<span class=\"badge badge-warning badge-pill\">"); client.print(feed_delay_small); client.println("</span></li>");
	client.print("            <li class=\"list-group-item d-flex justify-content-between align-items-center\">Time since last feed:<span class=\"badge badge-warning badge-pill\">"); client.print(time - feed_elapsed_small); client.println("</span></li>");
	client.println("          </ul>");
	client.println("        </div>");
	client.println("        <div class=\"col-md-4\">");
	client.println("          <ul class=\"list-group\">");
	client.println("            <li class=\"list-group-item d-flex justify-content-between align-items-center bg-warning\">Large Dog</li>");
	client.print("            <li class=\"list-group-item d-flex justify-content-between align-items-center\">Time between feeds:<span class=\"badge badge-warning badge-pill\">"); client.print(feed_delay_large); client.println("</span></li>");
	client.print("            <li class=\"list-group-item d-flex justify-content-between align-items-center\">Time since last feed:<span class=\"badge badge-warning badge-pill\">"); client.print(time - feed_elapsed_large); client.println("</span></li>");
	client.println("          </ul>");
	client.println("        </div>");
	client.println("      </div>");
	client.println("    </div>");
	client.println("    <div class=\"jumbotron jumbotron-fluid bg-dark text-light\">");
	client.println("      <div class=\"container\">");
	client.println("        <div class=\"row\">");
	client.println("          <div class=\"col-sm-8\">");
	client.println("            <h3>Manual Dispense</h3>");
	client.println("            <div class=\"btn-group btn-group-lg mt-2\" role=\"group\" aria-lable=\"Dispense Buttons\">");
	client.println("              <button id=\"manual-sm\" class=\"btn btn-warning border border-light\" onClick=\"dispense(1)\">Nibble</button>");
	client.println("              <button id=\"manual-ms\" class=\"btn btn-warning border border-light\" onClick=\"dispense(2)\">Snack</button>");
	client.println("              <button id=\"manual-md\" class=\"btn btn-warning border border-light\" onClick=\"dispense(3)\">Small Meal</button>");
	client.println("              <button id=\"manual-ml\" class=\"btn btn-warning border border-light\" onClick=\"dispense(4)\">Large Meal</button>");
	client.println("              <button id=\"manual-lg\" class=\"btn btn-warning border border-light\" onClick=\"dispense(5)\">Full Load</button>");
	client.println("            </div>");
	client.println("          </div>");
	client.println("          <div class=\"col-sm-4 text-right\">");
	client.println("            <h3>Scale Controls</h3>");
	client.println("            <div class=\"btn-group btn-group-lg mt-2\" role=\"group\" aria-lable=\"Tare Scale\">");
	client.println("              <button id=\"manual-sm\" class=\"btn btn-secondary border border-dark\" onClick=\"tare()\">Tare Scales</button>");
	client.println("            </div>");
	client.println("          </div>");
	client.println("        </div>");
	client.println("      </div>");
	client.println("    </div>");
	client.println("    <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>");
	client.println("    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\" crossorigin=\"anonymous\"></script>");
	client.println("    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script>");
	client.println("    <script>");
	client.println("      function dispense(num){");
	client.println("        var data = null");
	client.println("        var xhr = new XMLHttpRequest();");
	client.println("        xhr.withCredentials = true;");
	client.println("        xhr.addEventListener(\"readystatechange\", function () {");
	client.println("          if (this.readyState === 4) {");
	client.println("            console.log(this.responseText);");
	client.println("          }");
	client.println("        });");
	client.println("        xhr.open(\"POST\", \"/\");");
	client.println("        xhr.setRequestHeader(\"manual-dispense\", num);");
	client.println("        xhr.setRequestHeader(\"Cache-Control\", \"no-cache\");");
	client.println("        xhr.send(data)");
	client.println("      }");
	client.println("      function tare(){");
	client.println("        var data = null");
	client.println("        var xhr = new XMLHttpRequest();");
	client.println("        xhr.withCredentials = true;");
	client.println("        xhr.addEventListener(\"readystatechange\", function () {");
	client.println("          if (this.readyState === 4) {");
	client.println("            console.log(this.responseText);");
	client.println("          }");
	client.println("        });");
	client.println("        xhr.open(\"POST\", \"/\");");
	client.println("        xhr.setRequestHeader(\"scale-tare\", \"0\");");
	client.println("        xhr.setRequestHeader(\"Cache-Control\", \"no-cache\");");
	client.println("        xhr.send(data)");
	client.println("      }");
	client.println("    </script>");
	client.println("  </body>");
	client.println("</html>");
}

// FUNCTION:	server_parse
// INPUTS:		response
// OUTPUTS:		dispense
// RETURNS:		nill
// PURPOSE:		Parse any received POST requests
void server_parse() {
	// strncmp will stop at any '\n' or '\0' character, so we need to sanitise them from the response.
	int j;
	for (j = 0; j < 1024; j++) {
		if (response[j] == '\n') {
			response[j] = ' ';
		}
	}

	// Check the response for matches with valid POST requests. If a match is found, execute the required action.
	if (strstr(response, "manual-dispense: 1") != NULL) {
		feeder_dispense(1);
		printf("\n[FEEDER] Manual dispense triggered");
	}
	else if (strstr(response, "manual-dispense: 2") != NULL) {
		feeder_dispense(2);
		printf("\n[FEEDER] Manual dispense triggered");
	}
	else if (strstr(response, "manual-dispense: 3") != NULL) {
		feeder_dispense(3);
		printf("\n[FEEDER] Manual dispense triggered");
	}
	else if (strstr(response, "manual-dispense: 4") != NULL) {
		feeder_dispense(4);
		printf("\n[FEEDER] Manual dispense triggered");
	}
	else if (strstr(response, "manual-dispense: 5") != NULL) {
		feeder_dispense(5);
		printf("\n[FEEDER] Manual dispense triggered");
	}
	else if (strstr(response, "scale-tare") != NULL) {
		scale.tare();
		printf("\n[WEIGHT] Scales zeroed");
	}
}

// FUNCTION:	server
// INPUTS:		nill
// OUTPUTS:		nill
// RETURNS:		nill
// PURPOSE:		This runs the entire web server...
void server_http() {
	// This is a way to provide and end-user-facing interface to control the feeder, both to adjust times, and to manually override
	//   the system. It interfaces with the main part of the loop by setting and clearing flags for the main part to follow. This
	//   enables the code to be non-blocking (i.e.: fall though), as low latency is required for a speedy response to client queries
	//   on the shield.
	EthernetClient client = server.available();
	if (client) {

		// Ensure that the debug log is easily readable
		printf("\n");

		// A HTTP request ends with a blank line
		bool currentLineIsBlank = true;
		while (client.connected()) {
			if (client.available()) {
				// Get the response/request from the client, and print it to the serial console.
				char c = client.read();
				printf("%c", c);
				response[i] = c;
				i++;
				response[i] = '\0';
				
				if (c == '\n' && currentLineIsBlank) {
					i = 0;
					// Return a HTTP 200 (OK) response, with the content being a formatted HTML document
					server_http200(client);
					break;
				}
				if (c == '\n') {
					// you're starting a new line
					currentLineIsBlank = true;
				}
				else if (c != '\r') {
					// you've gotten a character on the current line
					currentLineIsBlank = false;
				}
			}
		}
		i = 0;
		server_parse();		/// Read the header for any POST requests
		delay(1);			/// give the web browser time to receive the data
		client.stop();		/// close the connection:
	}
}

// FUNCTION:	RX
// INPUTS:		via USART0
// OUTPUTS:		nill
// RETURNS:		scanf stream
// PURPOSE:		Pipes characters from the USART interface into the scanf function
static int RX(FILE *stream) {
	while ((UCSR0A & (1 << RXC0)) == 0) {};		/// Wait for receive flag to be set
	return(UDR0);								/// Return USART0 data register when it indicates that it is ready to be read.
}

// FUNCTION:	TX
// INPUTS:		printf stream
// OUTPUTS:		via USART0
// RETURNS:		nill
// PURPOSE:		Gets stream from scanf and passes into USART0 to be sent over Serial0
static int TX(char TXData, FILE *stream) {
	while ((UCSR0A & (1 << UDRE0)) == 0) {};	/// Wait for USART0 transmit register to indicate it is ready to transmit (flag)
	UDR0 = TXData;								/// Once flag indicates readiness, write byte to send into USART0 register.
	return 0;
}

// INTERRUPT:	Timer 1
// OUTPUTS:		temp, time
// PURPOSE:		Increment the temporary time. If temporary time is 50 (we are counting at 50Hz), then 1s has passed, so increment time.
ISR(TIMER1_OVF_vect) {
	temp++;
	if (temp > 49) {
		time++;
		temp = 0;
	}
}

// INTERRUPT:	Timer 5A
// OUTPUTS:		Timer 3B (servo)
// PURPOSE:		Reset the dispenser to its home position
ISR(TIMER5_COMPA_vect) {
	// If time has elapsed, reset the dispenser to home position
	OCR1B = LOAD_RESET;
	// We won't clear the flag yet, because we need time for more food to fall in to the drum. We will wait till Timer 5 Compare B to do that.
}

// INTERRUPT:	Timer 5B
// OUTPUTS:		feed
// PURPOSE:		Clear the timer feed flag now that a feed has finished to allow future feeds to occur
ISR(TIMER5_COMPB_vect) {
	feed = false;		/// Clear the "Feed Underway" flag.
	TIMSK5 = 0;			/// Disable interrupts associated with Timer 5 so that this won't keep resetting everything for future feeds.
}